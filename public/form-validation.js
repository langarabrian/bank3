// Example starter JavaScript for disabling form submissions if there are invalid fields
(function () {
  'use strict'

  window.addEventListener('load', function () {
    // Set up FeathersJS app
    const app = feathers()

    // Set up REST client
    const restClient = feathers.rest()

    // Configure an AJAX library with that client
    app.configure(restClient.fetch(window.fetch))

    // Connect to the `deposits` service
    const deposits = app.service('deposits')

    // Connect to the `deposits` service
    const balances = app.service('balances')

    async function updateBalances() {
      const allBalances = await balances.find()
      const balanceRows = allBalances.data.map( balance => "<tr><td>" + balance.name + "</td><td>" + balance.amount + "</td></tr>")
      document.getElementById('accounts').innerHTML = balanceRows.join('')
    }

    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.querySelectorAll('.needs-validation')

    const nameField = document.getElementById('accountName')
    const amountField = document.getElementById('amount')

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms)
      .forEach(function (form) {
        form.addEventListener('submit', async function (event) {
          event.preventDefault()
          event.stopPropagation()
          if (!form.checkValidity()) {
            console.log("Form is bad!")
            form.classList.add('was-validated')
          } else {
            console.log("Form is good!")
            form.classList.remove('was-validated')
            const accountName = nameField.value
            const amount = amountField.valueAsNumber
            console.log( accountName )
            console.log( amount )
            await deposits.create({
              name: accountName,
              amount
            })
            nameField.value = ""
            amountField.value = ""
            nameField.focus()
            updateBalances()
          }

        }, false)
      })
    updateBalances()
  }, false)
})()
