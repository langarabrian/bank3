// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default (options = {}): Hook => {
  return async (context: HookContext): Promise<HookContext> => {
    const { app, result } = context;
    const balanceService = app.service('balances');
    const balance = await balanceService.find({
      query: {
        name: result.name
      }
    });
    if ( balance.total == 0 ) {
      await balanceService.create({
        name: result.name,
        amount: result.amount
      });
    } else {
      await balanceService.patch( balance.data[0]._id, {
        amount: balance.data[0].amount + result.amount
      });
    }
    console.log( result );
    return context;
  };
};

function sleep(ms:number) {
  return new Promise(resolve => setTimeout(resolve,ms));
}
