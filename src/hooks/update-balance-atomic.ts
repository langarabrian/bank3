// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default (options = {}): Hook => {
  return async (context: HookContext): Promise<HookContext> => {
    const { app, result } = context;
    const balanceService = app.service('balances');
    const data = {
      name: result.name,
      $inc: { amount: result.amount }
    }
    const params = {
      query: { name: result.name },
      mongoose: { upsert: true }
    }
    await balanceService.patch( null, data, params ); 
    console.log( result );
    return context;
  };
};
