// Initializes the `deposits` service on path `/deposits`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Deposits } from './deposits.class';
import createModel from '../../models/deposits.model';
import hooks from './deposits.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'deposits': Deposits & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/deposits', new Deposits(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('deposits');

  service.hooks(hooks);
}
