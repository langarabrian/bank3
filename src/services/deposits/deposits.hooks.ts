import { HookContext } from '@feathersjs/feathers';
import { transactionManager } from 'feathers-mongoose';
import updateBalance from '../../hooks/update-balance';

import updateBalanceAtomic from '../../hooks/update-balance-atomic';

export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [],
//    create: [async (context: HookContext) => transactionManager.beginTransaction(context)],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [updateBalanceAtomic()],
//    create: [updateBalance()],
//    create: [updateBalance(),async (context: HookContext) => transactionManager.commitTransaction(context)],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
//    create: [async (context: HookContext) => transactionManager.rollbackTransaction(context)],
    update: [],
    patch: [],
    remove: []
  }
};
